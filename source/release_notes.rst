Release Notes
==========================================================================

.. toctree::
   :maxdepth: 1
   :titlesonly:
   :caption: Contents:

   md/release_notes/1_0.md
   md/release_notes/2_0.md
   md/release_notes/2_1.md