Architecture
==========================================================================

.. toctree::
   :maxdepth: 1
   :titlesonly:
   :caption: Contents:

   md/architecture/overview.md
   md/architecture/s3i_idp.md
   md/architecture/s3i_dir.md
   md/architecture/s3i_broker.md
   md/architecture/s3i_repo.md
   md/architecture/s3i_config.md